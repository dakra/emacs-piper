;;; SOURCE-ENVIRONMENT --- Read shell-specific variable files
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2019, Howard X. Abrams, all rights reserved.
;; Created: 28 October 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;  I noticed that many of my shell scripts would 'source' files that contained
;;  key value pairs that would assign variables into the environment. I wanted
;;  to have my Emacs scripts be able to read those same files, so I wrote a
;;  little function that would parse them.
;;
;;  To use, you can just `M-x source-environment' and point it to a file, and any
;;  KEY=VALUE pairs (one per line) will be added to environment.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defun source-environment--parser ()
  "Parses the current buffer for textual key=value pairs."
  (let ((kv-results (list))
        ;; This hairy regular expression matches KEY=VALUE shell expressions:
        (key-value-rx (rx
                       ;; Get the variable name from left side of = sign,
                       ;; which should begin with a letter or underbar:
                       (group (any alpha "_") (zero-or-more (any alnum "_")))
                       ;; The = sign can be surrounded with spaces:
                       (zero-or-more blank) "=" (zero-or-more blank)
                       ;; The value can be surrounded by quotes:
                       (group (optional (any "'" "\"")))
                       ;; The value can be just about anything (non-greedy):
                       (group (*? (any print)))
                       ;; The final optional quote should match first one
                       (optional (backref 2))
                       (zero-or-more blank)
                       line-end))

        ;; Expression to look for ${VARIABLES} to replace with a value:
        (variable-rx (rx "$" (zero-or-one "{")
                         (group (one-or-more (any alnum "_")))
                         (zero-or-one "}"))))

    ;; If a value on the right-hand side has a variable reference, like ${XYZ},
    ;; this mini-function to return its value from either the environment, from
    ;; a variable already defined in kv-results, or an empty string:
    (cl-flet ((matching-env-value (p)
                                  (let ((variable (match-string 1 p)))
                                    (or (getenv variable) (cdr (assoc variable kv-results)) ""))))

      (goto-char (point-min))
      (while (re-search-forward key-value-rx nil t)
        (let* ((key (match-string 1))
               (str-value (match-string 3)) ; Replace variables within this string value:
               (value (replace-regexp-in-string variable-rx #'matching-env-value str-value t)))
          (add-to-list 'kv-results (cons key value))))

      kv-results)))

(defun source-environment--store (tuple)
  "Given a cons, set the environment with the two parts."
  (let ((key (car tuple))
        (val (cdr tuple)))
    (setenv key val)
    (message "source-environment: Stored environment variable %s = %s" key val)))

(defun source-environment (file)
  "Add all environment variable settings from a script file into
the current Emacs environment, via the `setenv' function.

*Note:* This does not take the variables into Emacs variables,
just environment variables, so access them with `getenv', but
they should be accessible when other executables are called."
  (interactive "fSource file:")
  (save-excursion
    (with-temp-buffer
      (insert-file-contents file)
      (-map 'source-environment--store (source-environment--parser)))))


(provide 'source-environment)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; source-environment.el ends here
