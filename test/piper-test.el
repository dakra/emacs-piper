;;; PIPER-TESTS --- Test the non-executing piper functions
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2019, Howard X. Abrams, all rights reserved.
;; Created:  2 September 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defvar piper-test-path
  (f-dirname (f-this-file)))

(defvar piper-code-path
  (f-parent piper-test-path))

(require 'piper (f-expand "piper.el" piper-code-path))


(ert-deftest piper-test-range-of-numbers ()
  (should (equal (piper--range-of-numbers " ") '()))
  (should (equal (piper--range-of-numbers "1-3") '(1 2 3)))
  (should (equal (piper--range-of-numbers "-3") '(1 2 3)))
  (should (equal (piper--range-of-numbers "2,4, 6") '(2 4 6)))
  (should (equal (piper--range-of-numbers "-2, 4-6, 8") '(1 2 4 5 6 8)))
  (should (equal (piper--range-of-numbers "-2, 4 - 6, 8") '(1 2 4 5 6 8))))

(ert-deftest piper-test-column-lines-for-line ()
  (should (equal '("Hello" " 42")
                 (piper--column-lines-for-line "Hello, World, 42, foobar"
                                               "," '(1 3))))
  (should (equal '("Hello")
                 (piper--column-lines-for-line "Hello, World, 42, foobar"
                                               "," '(1))))
  (should (equal '("Hello" nil)
                 (piper--column-lines-for-line "Hello, World, 42, foobar"
                                               "," '(1 5))))
  (should (equal '("Hello" "42")
                 (piper--column-lines-for-line "Hello, World, 42, foobar"
                                               "," '(1 3) t)))
  (should (equal '("bar") (piper--column-lines-for-line "foo,,bar" "," '(3))))
  (should (equal '("") (piper--column-lines-for-line "foo,,bar" "," '(2)))))

(ert-deftest piper-test-keep-columns ()
  (should (equal (piper--keep-columns "foo, bar, 42, baz" "," '(1 3))
                 "foo, 42"))
  (should (equal (piper--keep-columns "foo, bar, 42, baz" "," '(3))
                 "42"))
  (should (equal (piper--keep-columns "foo, bar, 42, baz" "," '(1 3) t)
                 "foo,42"))
  (should (equal (piper--keep-columns "foo, bar, 42, baz" "," '())
                 "")))

(ert-deftest piper-test-command-parts ()
  (should (equal (piper--command-parts "bob")         '("bob" "bob" nil "*bob*")))
  (should (equal (piper--command-parts "bob.sh")      '("bob" "bob.sh" nil "*bob*")))
  (should (equal (piper--command-parts "bob bar baz") '("bob" "bob" ("bar" "baz") "*bob*")))
  (should (equal (piper--command-parts "~/foo/bob")   '("bob" "~/foo/bob" nil "*bob*")))
  (should (equal (piper--command-parts "~/foo/bob bling ding")
                 '("bob" "~/foo/bob" ("bling" "ding") "*bob*")))
  (should (equal (piper--command-parts "grep -r ham_reservation.csv ~/work/wpc4")
                 '("grep" "grep" ("-r" "ham_reservation.csv" "~/work/wpc4") "*grep*"))))

(ert-deftest piper-test-process-name-from-shell ()
  (should (equal (piper--process-name-from-shell "bob") "bob"))
  (should (equal (piper--process-name-from-shell "foo bar baz") "foo"))
  (should (equal (piper--process-name-from-shell "~/foo/bar/baz") "baz"))
  (should (equal (piper--process-name-from-shell "~/foo/bar/baz bog dog") "baz")))

(ert-deftest piper-test-replace-braces ()
  (should (equal (piper--replace-braces '("abc" "{}" "ghi") "def")
                 '("abc" "def" "ghi")))
  (should (equal (piper--replace-braces '("abc" "def" "gh{}") "i")
                 '("abc" "def" "ghi")))
  (should (equal (piper--replace-braces '("abc" "d{}f" "ghi") "e")
                 '("abc" "def" "ghi")))
  (should (equal (piper--replace-braces '("abc") "xyz") '("abc")))
  (should (equal (piper--replace-braces '() "xyz") '())))

(ert-deftest piper-test-replace-or-append ()
  (should (equal (piper--replace-or-append '("abc" "{}" "ghi") "def")
                 '("abc" "def" "ghi")))
  (should (equal (piper--replace-or-append '("abc" "d{}f" "ghi") "e")
                 '("abc" "def" "ghi")))
  (should (equal (piper--replace-or-append '("abc" "def") "ghi") '("abc" "def" "ghi"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; piper-tests.el ends here
