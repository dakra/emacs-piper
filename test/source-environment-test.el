;;; SOURCE-ENVIRONMENT-TEST --- Unit tests for `source-environment.el'
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2019, Howard X. Abrams, all rights reserved.
;; Created: 28 October 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defvar source-environment-test-path
  (f-dirname (f-this-file)))

(defvar source-environment-code-path
  (f-parent source-environment-test-path))

(require 'source-environment (f-expand "source-environment.el" source-environment-code-path))


(ert-deftest source-environment-test-simple ()
  (with-temp-buffer
    (insert "A=1\n")
    (insert "B = 2  \n")
    (insert "export C   = 3\n")
    (insert "D= '  4  '\n")
    (insert "E=\"5's\" ")

    (let ((results (source-environment--parser)))
      (should (equal "1" (cdr (assoc "A" results))))
      (should (equal "2" (cdr (assoc "B" results))))
      (should (equal "3" (cdr (assoc "C" results))))
      (should (equal "  4  " (cdr (assoc "D" results))))
      (should (equal "5's" (cdr (assoc "E" results)))) )))

(ert-deftest source-environment-test-replaces ()
  (with-temp-buffer
    (insert "A=y\n")
    (insert "B = x$A  \n")
    (insert "export C   = x${A}z \n")
    (insert "D=x$HOME")

    (let ((results (source-environment--parser)))
      (should (equal "y" (cdr (assoc "A" results))))
      (should (equal "xy" (cdr (assoc "B" results))))
      (should (equal "xyz" (cdr (assoc "C" results))))
      (should (s-starts-with? "x/" (cdr (assoc "D" results)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; source-environment-test.el ends here
